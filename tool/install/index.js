const {exec} = require('child_process')
const {promisify} = require('util')
const {red, greenBright} = require('chalk')

const execP = promisify(exec)

const commonTools = require('./common')
const devTools = require('./dev')

const install = async (tools= '') => {
    console.log(red.underline('Please wait!'))
    const res = execP(`npm i --save-dev ${tools}`)
    res.child.stdout
        .pipe(process.stdout)

    await res
    console.log(greenBright('Success!\n'))
}

exports.installCommon   = async () => install(commonTools)
exports.installDev      = async () => install(devTools)