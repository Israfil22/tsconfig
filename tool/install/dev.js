module.exports = [
    /*===== Lang =====*/
    [
        'typescript', '@types/node'
    ],
    /*===== Tool & Support =====*/
    [
        /*===== Builders =====*/
        ['gulp', 'gulp-typescript', 'gulp-ts-alias', "del"],
        /*===== Tests =====*/ 		      //Allows use es modules in tests
        ['jest', '@types/jest', 'babel-plugin-transform-es2015-modules-commonjs'],
        /*===== Linting =====*/
        ['eslint', '@typescript-eslint/eslint-plugin', '@typescript-eslint/parser']
    ]
].flat(Infinity).join(' ')