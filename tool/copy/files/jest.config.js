module.exports = {
    verbose: true,
    errorOnDeprecated: true,
    notify: true,
    notifyMode: 'always',
    testEnvironment: 'node',
    testMatch: [
        '**/__tests__/**/?(*.)+(spec|test).js',
        '**/?(*.)+(spec|test).js'
    ]
}
