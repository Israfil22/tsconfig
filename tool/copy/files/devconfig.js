const DIST_DIR  = 'dist'
const SRC_DIR   = 'src'
const BUILD_EXCLUDE_PREFIX = '_dev_'

module.exports = {
    DIST_DIR,
    SRC_DIR,
    BUILD_EXCLUDE_PREFIX
}