const {src, dest, parallel, series} = require('gulp')

const ts = require('gulp-typescript')
const tsAlias = require('gulp-ts-alias')
const del = require('del')

const {SRC_DIR, DIST_DIR,BUILD_EXCLUDE_PREFIX} = require('./devconfig')
const tsConfig = require('./tsconfig.json').compilerOptions

const tsProject = ts.createProject(tsConfig)



const EXCLUDE_SOURCE_PATH = `!${SRC_DIR}/**/${BUILD_EXCLUDE_PREFIX}*`

const clearBuildFolder = async () => {
    await del(DIST_DIR)
}
clearBuildFolder.displayName = 'Clear build folder'



const compileTypescript = async () => {
    return src([
        `${SRC_DIR}/**/*.ts`,
        EXCLUDE_SOURCE_PATH
    ])
        .pipe(tsAlias({
            configuration: tsProject.options
        }))
        .pipe(tsProject())
        .pipe(dest(DIST_DIR))
}
compileTypescript.displayName = 'Сompile typescript'



const copyExtensions = ['json', 'ejs']

const copyFiles = async () => {
    return src([
        ...copyExtensions.map(ext => `${SRC_DIR}/**/*.${ext}`),
        EXCLUDE_SOURCE_PATH
    ])
        .pipe(dest(DIST_DIR))
}
copyFiles.displayName = 'Copy files'



exports.default =
    series(
        clearBuildFolder,
        parallel(compileTypescript, copyFiles)
    )