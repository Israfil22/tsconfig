const {yellowBright} = require('chalk')
const utils = require('../../../utils')
    const prompt = utils.prompt

module.exports = async (file) => {
    if (file.name === 'package.json'){
        const packageName = await prompt(yellowBright('Enter package name'))
        file.content = file.content.replace(/%PROJECT-NAME%/g, packageName)
    }
    return file
}