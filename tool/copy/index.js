const {greenBright} = require('chalk')
const {join} = require('path')
const fs = require('fs')
const transforms = require('./transforms')
const main = require('../../')

const copyFromPath = join(__dirname, '/files')
const copyDestPath = join(main.executeRootDir, '/')

module.exports = async () => {
    const copyDir = await fs.promises.opendir(copyFromPath)

    const filesToCopy = []

    for await (const entry of copyDir) {
        if (entry.isFile())
            filesToCopy.push(entry.name)
    }

    const filesPaths = filesToCopy
        .map(file => {return {
            path: join(copyFromPath, file),
            name: file
        }})

    for (let file of filesPaths) {
        file = {
            ...file,
            content: await fs.promises.readFile(file.path, {encoding: 'utf-8'})
        }

        for (const transform of transforms)
            file = await transform(file)

        const destPath = join(copyDestPath, file.name)

        await fs.promises.writeFile(destPath, file.content, {encoding: 'utf-8'})

        console.log(greenBright(`Successfully copied ${file.name}.`))
    }
}