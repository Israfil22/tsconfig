const readline = require('readline')

exports.prompt = async (question, delimiter = ": ") => {
    const consoleInterface = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    })

    return new Promise(resolve => {
        consoleInterface.question(question + delimiter, (result) => {
            resolve(result)
            consoleInterface.close()
        })
    })
}