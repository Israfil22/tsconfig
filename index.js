#!/usr/bin/env node
const {join} = require('path')
const {magenta, cyan} = require('chalk')

exports.executeRootDir = process.cwd()
exports.toolRootDir = join(__dirname, '/tool/')

const depsInstall = require('./tool/install')
const copy = require('./tool/copy')

const executeOperation = async (header, task, chalk) => {
	console.log(chalk(`Start: ${header}...`))
	await task()
	console.log(chalk('Done!\n'))
}

const main = async () => {
	await executeOperation('copying...', copy, magenta)

	await executeOperation('common dependencies installing', depsInstall.installCommon, magenta)

	await executeOperation('development dependencies installing', depsInstall.installDev, magenta)

	console.log(cyan('Complete !'))
}

main()
